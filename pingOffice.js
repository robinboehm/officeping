//#!/usr/bin/env node

var exec = require('child_process').exec;
var request = require("request-json");

var client = request.newClient('http://localhost:8888/');

function puts(error, stdout, stderr) {
  delete require.cache[require.resolve('./nameMappings.json')];
  var nameMappings = require('./nameMappings.json');

  var allIps = /([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/gm;


  var result = stdout.match(allIps);

  var onlineOfficeUsers = {};

  for (var i = 0; i < result.length; i++) {
    onlineOfficeUsers[result[i]] = true;
  }

  var resultArray = [];
  for (ip in nameMappings) {
    if (onlineOfficeUsers[ip]) {
      resultArray.push(nameMappings[ip]);
    }
  }
  //
  var webHookPayload = {
    text: 'has spotted some nerds: ' + resultArray.join(', '),
    username: 'Manfred the invisible Office-Ant',
    icon_emoji: ':ant:'
  };

  var client = request.newClient('http://localhost:8888/');

  client.post('https://symetics.slack.com/services/hooks/incoming-webhook?token=KnVwnkETlO5LOY8gTr7eGop2', webHookPayload, function (err, res, body) {
      return console.log(res.statusCode);
    }
  );


}

exec("ping -c 2 192.168.1.255", puts);
